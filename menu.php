<nav>
    <ul>
        <li><a href="index.php">Úvod</a></li>
        <li><a href="clanky.php">Články</a></li>
        <li><a href="kontakt.php">Kontakt</a></li>

        <?php
            if (isset($_SESSION['iduser'])){
                echo '<li><a href="logout.php">Logout</a></li>';
                echo '<li><a href="admin.php">Admin</a></li>';
            } else {
                echo '<li><a href="login.php">Login</a></li>';
            }
        ?>
    </ul>
</nav>