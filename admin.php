<?php require_once('head.php');?>
<?php require_once('fce/check_permission.php');?>

<body>
<header>
    <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
    <?php include('menu.php');?>
</header>

<div class="content">

    <h2>Admin sekce</h2>
    <p>Vítej <?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?></p>
    <?php include('menu-admin.php');?>

    <!--
        - Úprava profilu
        - Založit nový článek
        - Úprava stávajících článků
    -->


</div>
<?php include('footer.php');?>

</body>
</html>