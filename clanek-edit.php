<?php require_once('head.php');?>
<?php require_once('fce/check_permission.php');?>

<body>
<header>
    <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
    <?php include('menu.php');?>
</header>

<div class="content">

    <h2>Admin sekce</h2>
    <p>Vítej <?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?></p>
    <?php include('menu-admin.php');?>

    <?php
        $idclanek = $_GET['idclanek'];
        echo "Id článku pro edit je: " . $idclanek;

    ?>

</div>
<?php include('footer.php');?>

</body>
</html>